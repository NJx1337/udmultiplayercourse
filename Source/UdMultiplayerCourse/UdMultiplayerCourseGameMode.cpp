// Copyright Epic Games, Inc. All Rights Reserved.

#include "UdMultiplayerCourseGameMode.h"
#include "UdMultiplayerCourseCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUdMultiplayerCourseGameMode::AUdMultiplayerCourseGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
